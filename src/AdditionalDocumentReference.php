<?php

namespace Striktly\UBL\Invoice;


use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

class AdditionalDocumentReference implements XmlSerializable
{
    private $UBLVersion;
    private $id;
    private $attachment;
    private $filename;

    /**
     * AdditionalDocumentReference constructor.
     * @param $UBLVersion
     */
    public function __construct($UBLVersion = '2.1')
    {
        $this->UBLVersion = $UBLVersion;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AdditionalDocumentReference
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param $attachment
     * @return AdditionalDocumentReference
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param $filename
     * @return AdditionalDocumentReference
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    function xmlSerialize(Writer $writer)
    {
        switch ($this->UBLVersion) {
            case '2.1':
                $writer->write([
                    Schema::CBC . 'ID' => $this->id,
                    Schema::CAC . 'Attachment' => [
                        [
                            'name' => Schema::CBC . 'EmbeddedDocumentBinaryObject',
                            'value' => $this->attachment,
                            'attributes' => [
                                'mimeCode' => "application/pdf",
                                'filename' => $this->filename
                            ]]
                    ]
                ]);
                break;

            case 'eFFF':
                $writer->write([
                        Schema::CBC . 'ID' => $this->id,
                        Schema::CBC . 'DocumentType' => 'CommercialInvoice',
                        Schema::CAC . 'Attachment' => [
                            [
                                'name' => Schema::CBC . 'EmbeddedDocumentBinaryObject',
                                'value' => $this->attachment,
                                'attributes' => [
                                    'mimeCode' => "application/pdf",
                                    'filename' => $this->filename
                                ]]
                        ]
                    ]
                );
                break;
        }

    }
}
