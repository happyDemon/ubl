<?php


namespace Striktly\UBL\Invoice;


use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

class Country implements XmlSerializable
{
    private $identificationCode;
    private $UBLVersion;

    /**
     * Country constructor.
     * @param $UBLVersion
     */
    public function __construct($UBLVersion = '2.1')
    {
        $this->UBLVersion = $UBLVersion;
    }


    /**
     * @return mixed
     */
    public function getIdentificationCode()
    {
        return $this->identificationCode;
    }

    /**
     * @param mixed $identificationCode
     * @return Country
     */
    public function setIdentificationCode($identificationCode)
    {
        $this->identificationCode = $identificationCode;
        return $this;
    }


    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    function xmlSerialize(Writer $writer)
    {
        switch ($this->UBLVersion) {
            case '2.1':
                $writer->write([
                    Schema::CBC . 'IdentificationCode' => $this->identificationCode,
                ]);
                break;

            case 'eFFF':
                $writer->write([
                    Schema::CBC . 'IdentificationCode' => [
                        'value' => $this->identificationCode,
                        'attributes' => [
                            'listID' => "ISO3166-1:Alpha2",
                        ]],
                ]);
                break;
        }

    }


}