<?php


namespace Striktly\UBL\Invoice;


use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

class Item implements XmlSerializable
{
    private $description;
    private $name;
    private $sellersItemIdentification;
    private $classifiedTaxCategory;
    private $UBLVersion;

    /**
     * Item constructor.
     * @param $UBLVersion
     */
    public function __construct($UBLVersion = '2.1')
    {
        $this->UBLVersion = $UBLVersion;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSellersItemIdentification()
    {
        return $this->sellersItemIdentification;
    }

    /**
     * @param mixed $sellersItemIdentification
     * @return Item
     */
    public function setSellersItemIdentification($sellersItemIdentification)
    {
        $this->sellersItemIdentification = $sellersItemIdentification;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClassifiedTaxCategory()
    {
        return $this->classifiedTaxCategory;
    }

    /**
     * @param mixed $classifiedTaxCategory
     */
    public function setClassifiedTaxCategory($classifiedTaxCategory)
    {
        $this->classifiedTaxCategory = $classifiedTaxCategory;
    }


    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    function xmlSerialize(Writer $writer)
    {
        switch ($this->UBLVersion) {
            case '2.1':
                $writer->write([
                    Schema::CBC . 'Description' => $this->description,
                    Schema::CBC . 'Name' => $this->name,
                    Schema::CAC . 'SellersItemIdentification' => [
                        Schema::CBC . 'ID' => $this->sellersItemIdentification
                    ],
                ]);
                break;

            case 'eFFF':
                $writer->write([
                    Schema::CBC . 'Description' => $this->description,
                    Schema::CBC . 'Name' => $this->name,
                    Schema::CAC . 'SellersItemIdentification' => [
                        Schema::CBC . 'ID' => $this->sellersItemIdentification
                    ],
                    Schema::CAC . 'ClassifiedTaxCategory' => $this->classifiedTaxCategory
                ]);
                break;

        }

    }
}