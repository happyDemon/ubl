<?php


namespace Striktly\UBL\Invoice;

use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

class LegalEntity implements XmlSerializable
{

    /**
     * @var string
     */
    private $registrationName;

    /**
     * @var int
     */
    private $companyId;

    private $UBLVersion;

    /**
     * LegalEntity constructor.
     * @param $UBLVersion
     */
    public function __construct($UBLVersion = '2.1')
    {
        $this->UBLVersion = $UBLVersion;
    }


    public function getRegistrationName()
    {
        return $this->registrationName;
    }

    public function setRegistrationName($registrationName)
    {
        $this->registrationName = $registrationName;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    function xmlSerialize(Writer $writer)
    {
        switch ($this->UBLVersion) {
            case '2.1':
                $writer->write([
                    Schema::CBC . 'RegistrationName' => $this->registrationName,
                    Schema::CBC . 'CompanyID' => $this->companyId
                ]);
                break;

            case 'eFFF':
                $vat = str_replace('BE', '', $this->companyId);
                $writer->write([
                    Schema::CBC . 'RegistrationName' => $this->registrationName,
                    Schema::CBC . 'CompanyID' => [
                        'value' => $vat,
                        'attributes' => [
                            'schemeID' => 'BE:CBE'
                        ]
                    ]
                ]);
                break;
        }

    }
}