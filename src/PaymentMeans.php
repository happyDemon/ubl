<?php


namespace Striktly\UBL\Invoice;


use Striktly\UBL\Invoice\Schema;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

class PaymentMeans implements XmlSerializable
{
    private $paymentMeansCode;

    private $instructionId;

    private $UBLVersion;

    /**
     * PaymentMeans constructor.
     * @param $UBLVersion
     */
    public function __construct($UBLVersion = '2.1')
    {
        $this->UBLVersion = $UBLVersion;
    }


    /**
     * @return mixed
     */
    public function getPaymentMeansCode()
    {
        return $this->paymentMeansCode;
    }

    /**
     * @param mixed $paymentMeansCode
     */
    public function setPaymentMeansCode($paymentMeansCode)
    {
        $this->paymentMeansCode = $paymentMeansCode;
    }

    /**
     * @return mixed
     */
    public function getInstructionId()
    {
        return $this->instructionId;
    }

    /**
     * @param mixed $instructionId
     */
    public function setInstructionId($instructionId)
    {
        $this->instructionId = $instructionId;
    }


    /**
     * The xmlSerialize method is called during xml writing.
     *
     * Use the $writer argument to write its own xml serialization.
     *
     * An important note: do _not_ create a parent element. Any element
     * implementing XmlSerializble should only ever write what's considered
     * its 'inner xml'.
     *
     * The parent of the current element is responsible for writing a
     * containing element.
     *
     * This allows serializers to be re-used for different element names.
     *
     * If you are opening new elements, you must also close them again.
     *
     * @param Writer $writer
     * @return void
     */
    function xmlSerialize(Writer $writer)
    {
        switch ($this->UBLVersion) {
            case '2.1':
                $writer->write([
                    Schema::CBC . 'PaymentMeansCode' => $this->paymentMeansCode
                ]);

                if ($this->instructionId !== null) {
                    $writer->write([
                        Schema::CBC . 'InstructionID' => $this->instructionId
                    ]);
                }
                break;
            case 'eFFF':
                $writer->write([
                    Schema::CBC . 'PaymentMeansCode' => [
                        "value" => 1,
                        "attributes" => [
                            "listID" => "UNCL4461"
                        ]
                    ]
                ]);

                if ($this->instructionId !== null) {
                    $writer->write([
                        Schema::CBC . 'InstructionID' => $this->instructionId
                    ]);
                }
                break;
        }

    }
}