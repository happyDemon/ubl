<?php


namespace Striktly\UBL\Invoice;


use Striktly\UBL\Invoice\Schema;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

class PaymentTerms implements XmlSerializable
{
    private $note;
    private $amount;
    private $settlementDiscountPercent;
    private $settlementPeriod;
    private $penaltySurchargePercent;
    private $penaltyPeriod;

    /**
     * @return mixed
     */
    public function getPenaltySurchargePercent()
    {
        return $this->penaltySurchargePercent;
    }

    /**
     * @param mixed $penaltySurchargePercent
     */
    public function setPenaltySurchargePercent($penaltySurchargePercent)
    {
        $this->penaltySurchargePercent = $penaltySurchargePercent;
    }

    /**
     * @return mixed
     */
    public function getPenaltyPeriod()
    {
        return $this->penaltyPeriod;
    }

    /**
     * @param mixed $penaltyPeriod
     */
    public function setPenaltyPeriod($penaltyPeriod)
    {
        $this->penaltyPeriod = $penaltyPeriod;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getSettlementDiscountPercent()
    {
        return $this->settlementDiscountPercent;
    }

    /**
     * @param mixed $settlementDiscountPercent
     */
    public function setSettlementDiscountPercent($settlementDiscountPercent)
    {
        $this->settlementDiscountPercent = $settlementDiscountPercent;
    }


    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return PeriodType
     */
    public function getSettlementPeriod()
    {
        return $this->settlementPeriod;
    }

    /**
     * @param PeriodType $settlementPeriod
     */
    public function setSettlementPeriod($settlementPeriod)
    {
        $this->settlementPeriod = $settlementPeriod;
    }


    /**
     * The xmlSerialize method is called during xml writing.
     *
     * Use the $writer argument to write its own xml serialization.
     *
     * An important note: do _not_ create a parent element. Any element
     * implementing XmlSerializble should only ever write what's considered
     * its 'inner xml'.
     *
     * The parent of the current element is responsible for writing a
     * containing element.
     *
     * This allows serializers to be re-used for different element names.
     *
     * If you are opening new elements, you must also close them again.
     *
     * @param Writer $writer
     * @return void
     */
    function xmlSerialize(Writer $writer)
    {
        if ($this->note !== null) {
            $writer->write([
                Schema::CBC . 'Note' => $this->note
            ]);
        }
        if ($this->settlementDiscountPercent !== null) {
            $writer->write([
                Schema::CBC . 'SettlementDiscountPercent' => $this->settlementDiscountPercent
            ]);
        }
        if ($this->amount !== null) {
            $writer->write([
                'name' => Schema::CBC . 'Amount',
                'value' => number_format($this->amount, 2, '.', ''),
                'attributes' => [
                    'currencyID' => Generator::$currencyID
                ]
            ]);
        }
        if ($this->settlementPeriod !== null) {
            $writer->write([
                Schema::CAC . 'SettlementPeriod' => $this->settlementPeriod
            ]);
        }
        if ($this->penaltySurchargePercent !== null) {
            $writer->write([
                Schema::CBC . 'PenaltySurchargePercent' => $this->penaltySurchargePercent
            ]);
        }
        if ($this->penaltyPeriod !== null) {
            $writer->write([
                Schema::CAC . 'PenaltyPeriod' => $this->penaltyPeriod
            ]);
        }
    }
}