<?php


namespace Striktly\UBL\Invoice;


use Striktly\UBL\Invoice\Schema;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlSerializable;

class PeriodType implements XmlSerializable
{
    private $startDate;
    private $startTime;
    private $endDate;
    private $endTime;

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param mixed $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * The xmlSerialize method is called during xml writing.
     *
     * Use the $writer argument to write its own xml serialization.
     *
     * An important note: do _not_ create a parent element. Any element
     * implementing XmlSerializble should only ever write what's considered
     * its 'inner xml'.
     *
     * The parent of the current element is responsible for writing a
     * containing element.
     *
     * This allows serializers to be re-used for different element names.
     *
     * If you are opening new elements, you must also close them again.
     *
     * @param Writer $writer
     * @return void
     */
    function xmlSerialize(Writer $writer)
    {
        if ($this->startDate !== null) {
            $writer->write([
                Schema::CBC . 'StartDate' => $this->startDate
            ]);
        }
        if ($this->startTime !== null) {
            $writer->write([
                Schema::CBC . 'StartTime' => $this->startTime
            ]);
        }
        if ($this->endDate !== null) {
            $writer->write([
                Schema::CBC . 'EndDate' => $this->endDate
            ]);
        }
        if ($this->endTime !== null) {
            $writer->write([
                Schema::CBC . 'EndTime' => $this->endTime
            ]);
        }
    }
}